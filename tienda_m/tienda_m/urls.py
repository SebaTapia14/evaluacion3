"""tienda_m URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from tienda_m.views.perruqueria import perruqueria
from tienda_m.views.producto import producto_perruqueria
from tienda_m.views.contacto import contacto_perruqueria, formulario_contacto
from tienda_m.views.miusuario import load_contacto
from tienda_m.views.crearusuario import crearusuario_perruqueria


urlpatterns = [
    path('admin/', admin.site.urls),
    path('perruqueria/', perruqueria),
    path('', perruqueria),
    path('producto/', producto_perruqueria),
    path('contacto/', contacto_perruqueria),
    path('contacto/formulario', formulario_contacto),  
    path('mi-usuario/', load_contacto),  
    path('crearusuario/', crearusuario_perruqueria)
]
