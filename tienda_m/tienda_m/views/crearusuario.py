from django.shortcuts import render
from tienda_m.models import Contacto

def crearusuario_perruqueria(request):
    print('crearusuario_perruqueria')
    return render(request, 'crearusuario.html')

def contacto_crearusuario(request):
    print('contacto_crearusuario')

    if request.method == 'GET':
        print('invocación por método GET')
        run = request.GET.get('run')
        print('run {0}'.format(run))

    elif request.method == 'POST':
        print('invocación por método POST')
        email = request.POST.get('email')
        contraseña = request.POST.get('contraseña')
        rcontraseña = request.POST.get('rcontraseña')
        nombres = request.POST.get('nombres')
        apellidos = request.POST.get('apellidos')
        run = request.POST.get('run')
        dv = request.POST.get('dv')
        dia_nacimiento = request.POST.get('fecha_nacimiento')
        mes_nacimiento = request.POST.get('mes_nacimiento')
        anio_nacimiento = request.POST.get('anio_nacimiento')
        region = request.POST.get('region')
        comuna = request.POST.get('comuna')
        ciudad = request.POST.get('ciudad')

        ##Crear un objeto Registro
        ##que posee relacion con la tabla registro
        registro = Contacto()
        registro.email = email
        registro.contraseña = contraseña
        registro.rcontraseña = rcontraseña
        registro.nombres = nombres
        registro.apellidos = apellidos
        registro.run = run
        registro.dv = dv
        registro.dia_nacimiento = dia_nacimiento
        registro.mes_nacimiento = mes_nacimiento
        registro.anio_nacimiento = anio_nacimiento
        registro.region = region
        registro.comuna = comuna
        registro.ciudad = ciudad
        registro.save()
        ##print('run {0}'.format(run))

    return render(request, 'registro.html')